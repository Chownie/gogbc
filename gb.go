package main

import (
	"encoding/binary"
	"fmt"
)

// GameBoy emulator struct
type GameBoy struct {
	Register  	Registers
	Memory    	Memory
	Ticks	  	int
	Interrupt 	Interrupt
	GPU			GPU
}

var instructionTicks [256]int = [256]int{ 
	2, 6, 4, 4, 2, 2, 4, 4, 10, 4, 4, 4, 2, 2, 4, 4, // 0x0_
	2, 6, 4, 4, 2, 2, 4, 4,  4, 4, 4, 4, 2, 2, 4, 4, // 0x1_
	0, 6, 4, 4, 2, 2, 4, 2,  0, 4, 4, 4, 2, 2, 4, 2, // 0x2_
	4, 6, 4, 4, 6, 6, 6, 2,  0, 4, 4, 4, 2, 2, 4, 2, // 0x3_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x4_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x5_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x6_
	4, 4, 4, 4, 4, 4, 2, 4,  2, 2, 2, 2, 2, 2, 4, 2, // 0x7_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x8_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x9_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0xa_
	2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0xb_
	0, 6, 0, 6, 0, 8, 4, 8,  0, 2, 0, 0, 0, 6, 4, 8, // 0xc_
	0, 6, 0, 0, 0, 8, 4, 8,  0, 8, 0, 0, 0, 0, 4, 8, // 0xd_
	6, 6, 4, 0, 0, 8, 4, 8,  8, 2, 8, 0, 0, 0, 4, 8, // 0xe_
	6, 6, 4, 2, 0, 8, 4, 8,  6, 4, 8, 2, 0, 0, 4, 8} // 0xf_

// getShort composes together two bytes
func composeShort(a uint8, b uint8) uint16 {
	return binary.LittleEndian.Uint16([]uint8{a, b})
}

func splitShort(value uint16) (uint8, uint8) {
	return uint8(value & 0xFF), uint8(value >> 8)
}

func (gb *GameBoy) getByte(pointer uint16) uint8 {
	return gb.Memory.ReadByte(pointer)
}

func (gb *GameBoy) peekByte() uint8 {
	return gb.getByte(gb.Register.PC)
}

func (gb *GameBoy) nextByte() uint8 {
	val := gb.peekByte()
	gb.Register.PC++
	return val
}

func (gb *GameBoy) getShort(pointer uint16) uint16 {
	return gb.Memory.ReadShort(pointer)
}

func (gb *GameBoy) peekShort() uint16 {
	return gb.getShort(gb.Register.PC)
}

func (gb *GameBoy) nextShort() uint16 {
	val := gb.peekShort()
	gb.Register.PC += 2
	return val
}

func (gb *GameBoy) Reset() {
	// Reset memory to blank
	gb.Memory = Memory{}

	// Set default register state
	gb.Register.A = 0x01
	gb.Register.F = 0xB0
	gb.Register.B = 0x00
	gb.Register.C = 0x13
	gb.Register.D = 0x00
	gb.Register.E = 0xD8
	gb.Register.H = 0x01
	gb.Register.L = 0x4D
	gb.Register.SP = 0xFFFE
	gb.Register.PC = 0x100

	gb.Interrupt.Master = 1
	gb.Interrupt.Enable = 0
	gb.Interrupt.Flags = 0

	gb.Ticks = 0
}

func (gb *GameBoy) LoadROM(ROM []byte) {
	gb.Reset()
	gb.Memory.Write(0, ROM)
	fmt.Println("Game name: ", string(ROM[0x0134:0x0142]))
	fmt.Println("Rom type: ", ROM[0x147])
}

// Execute the ROM in order
func (gb *GameBoy) Execute() {
	if gb.Register.PC == 0x2817 || gb.Register.PC == 0x282a  {
		output := gb.Memory.GetVRAM()
		nonZeros := 0
		for _, value := range output[:64] {
			if value != 0 {
				nonZeros += 1
			}
		}
		fmt.Printf("NON-ZERO CELLS IN FIRST VRAM: %d\n", nonZeros)
	}
	input := gb.getByte(gb.Register.PC)
	fmt.Printf("%#04x:\t%#02x\t\t%#02x\n", gb.Register.PC, input, gb.Memory.ReadShort(gb.Register.PC))
	gb.Register.PC++
	if gb.Register.PC == 0x2817 || gb.Register.PC == 0x282a {
		fmt.Println(gb.Memory.GetVRAM())
		return
	}
	switch input {
	// NOP
	case 0x00:
		gb.nop()
	case 0x76:
		gb.halt()
	case 0x8E:
		gb.adc_hl()
	case 0x3E:
		gb.ld_byte_n(&gb.Register.A)
	case 0x04:
		gb.inc(&gb.Register.B)
	case 0x0C:
		gb.inc(&gb.Register.C)
	case 0x14:
		gb.inc(&gb.Register.D)
	case 0x10:
		gb.stop()
	case 0x16:
		gb.ld_byte_n(&gb.Register.D)
	case 0x07:
		gb.rlc(&gb.Register.A)
	case 0x0F:
		gb.rrc(&gb.Register.A)
	case 0x1F:
		gb.rra()
	case 0x20:
		gb.jr_nz_n() // Jump when Z is 0
	case 0x21:
		gb.ld_union_n(&gb.Register.H, &gb.Register.L)

	/*
	 * LD A, byte
	 */
	case 0x78:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.B)
	case 0x79:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.C)
	case 0x7A:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.D)
	case 0x7B:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.E)
	case 0x7C:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.H)
	case 0x7D:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.L)
	case 0x7F:
		gb.ld_byte_byte(&gb.Register.A, &gb.Register.A)

	/*
	 * LD B, byte
	 */
	case 0x40:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.B)
	case 0x41:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.C)
	case 0x42:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.D)
	case 0x43:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.E)
	case 0x44:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.H)
	case 0x45:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.L)
	case 0x46:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.B)
	case 0x47:
		gb.ld_byte_byte(&gb.Register.B, &gb.Register.A)
	case 0x06:
		gb.ld_byte_n(&gb.Register.B)
	/*
	 * LD C, byte
	 */
	case 0x0E:
		gb.ld_byte_n(&gb.Register.C)
	case 0x48:
		gb.ld_byte_byte(&gb.Register.C, &gb.Register.B)
	case 0x49:
		gb.ld_byte_byte(&gb.Register.C, &gb.Register.C)
	/*
	 * LD H, byte
	 */
	case 0x60:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.B)
	case 0x61:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.C)
	case 0x62:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.D)
	case 0x63:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.E)
	case 0x64:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.H)
	case 0x65:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.L)
	case 0x66:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.H)
	case 0x67:
		gb.ld_byte_byte(&gb.Register.H, &gb.Register.A)
	case 0x26:
		gb.ld_byte_n(&gb.Register.H)

	/*
	 * LD L, byte
	 */
	case 0x68:
		gb.ld_byte_byte(&gb.Register.L, &gb.Register.B)
	case 0x6A:
		gb.ld_byte_byte(&gb.Register.L, &gb.Register.D)

	/*
	 * LD byte, (HL)
	 */
	case 0x7E:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.A)
	case 0x4E:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.C)
	case 0x56:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.D)
	case 0x5E:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.E)
	case 0x6E:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.L)

	/*
	 * ADD byte, byte
	 */
	case 0x87:
		gb.add_byte(&gb.Register.A, &gb.Register.A)
	case 0x80:
		gb.add_byte(&gb.Register.A, &gb.Register.B)
	case 0x81:
		gb.add_byte(&gb.Register.A, &gb.Register.C)
	case 0x82:
		gb.add_byte(&gb.Register.A, &gb.Register.D)
	case 0x83:
		gb.add_byte(&gb.Register.A, &gb.Register.E)
	case 0x84:
		gb.add_byte(&gb.Register.A, &gb.Register.H)
	case 0x85:
		gb.add_byte(&gb.Register.A, &gb.Register.L)

	/*
	 *
	 */
	case 0xF3:
		gb.di()
	case 0xC9:
		gb.ret()
	case 0xB4:
		gb.or(&gb.Register.H)
	case 0x2F:
		gb.cpl()
	case 0x34:
		gb.inc_short(&gb.Register.H, &gb.Register.L)
	case 0x12:
		gb.ld_addr_nn_byte(&gb.Register.D, &gb.Register.E, &gb.Register.A)
	case 0x17:
		gb.rlc(&gb.Register.A)
	case 0x37:
		gb.scf()
	case 0x33:
		gb.inc_short_addr(&gb.Register.SP)
	case 0x35:
		gb.dec_short(&gb.Register.H, &gb.Register.L)
	case 0x2B:
		gb.dec_hl()
	case 0x38:
		gb.jr_c_n()
	case 0x39:
		gb.add_short_addr(&gb.Register.SP)
	case 0x3A:
		gb.ld_addr_nn_byte(&gb.Register.H, &gb.Register.L, &gb.Register.A)
		gb.dec_hl()
	case 0x3B:
		gb.dec_short_addr(&gb.Register.SP)
	case 0x3C:
		gb.inc(&gb.Register.A)

	case 0x3F:
		gb.ccf()
	case 0x11:
		gb.ld_union_n(&gb.Register.D, &gb.Register.E)

	/*
	 * LD (dd), A
	 */
	case 0x02:
		gb.ld_comp_addr_A(&gb.Register.B, &gb.Register.C)
	case 0x32:
		gb.ld_comp_addr_A(&gb.Register.H, &gb.Register.L)
		gb.dec_short(&gb.Register.H, &gb.Register.L)
	case 0x77:
		gb.ld_comp_addr_A(&gb.Register.H, &gb.Register.L)
	/*
	* DEC n
	*/
	case 0x3D:
		gb.dec(&gb.Register.A)
	case 0x05:
		gb.dec(&gb.Register.B)
	case 0x0D:
		gb.dec(&gb.Register.C)
	case 0x15:
		gb.dec(&gb.Register.D)
	case 0x1D:
		gb.dec(&gb.Register.E)
	case 0x25:
		gb.dec(&gb.Register.H)
	case 0x2D:
		gb.dec(&gb.Register.L)
	case 0x29:
		gb.add_short(&gb.Register.H, &gb.Register.L)
	case 0x19:
		gb.add_short(&gb.Register.D, &gb.Register.E)
	case 0x30:
		gb.jr_nc_n()
	case 0x31:
		gb.ld_short_nn(&gb.Register.SP)
	case 0x6F:
		gb.ld_byte_byte(&gb.Register.L, &gb.Register.A)
	case 0x97:
		gb.sub_addr(&gb.Register.A)
	case 0xAF:
		gb.xor(&gb.Register.A, gb.Register.A)
	case 0xB0:
		gb.or(&gb.Register.B)
	case 0xB2:
		gb.or(&gb.Register.D)
	case 0xC3:
		nn := gb.nextShort()
		gb.jp_nn(nn)
	case 0x08:
		gb.ld_sp_addr()
	case 0xCD:
		nn := gb.nextShort()
		gb.call_nn(nn)
	case 0xBE:
		gb.cp_addr(&gb.Register.H, &gb.Register.L)
	case 0xBF:
		gb.cp(&gb.Register.A)
	case 0xD2:
		gb.jp_cc_nn(C, false)
	case 0xE0:
		gb.ldh_n_a()
	case 0xF0:
		gb.ldh_a_n()
	case 0xFE:
		val := gb.nextByte()
		gb.cp_addr(&gb.Register.A, &val)
	case 0xFA:
		gb.ld_nn_addr_a()
	case 0x18:
		gb.jr_n()
	case 0x36:
		gb.ld_addr_ram(&gb.Register.H, &gb.Register.L)
	case 0xEA:
		gb.ld_nn_addr_a()
	case 0xE2:
		gb.ldh_byteaddr_a(&gb.Register.C)
	case 0x2A:
		gb.ld_addr_ram(&gb.Register.H, &gb.Register.L)
		gb.inc_short(&gb.Register.H, &gb.Register.L)
	case 0x2C:
		gb.inc(&gb.Register.L)
	default:
		output := fmt.Sprintf("UNIMPLEMENTED OPCODE %#02x | %#02x AT %#02x\n", input, gb.Memory.ReadShort(gb.Register.PC), gb.Register.PC)
		panic(output)
	}

	gb.Ticks += instructionTicks[input]
	gb.GPU.Step(gb.Ticks)
}
