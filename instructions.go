package main

import (
	"fmt"
	"math/bits"
	"time"
)

func (gb *GameBoy) nop() {
	return
}

func (gb *GameBoy) stop() {
	time.Sleep(3000)
}

func (gb *GameBoy) halt() {
	fmt.Println("HALTING")
	//if gb.Interrupt.Master == 1 {
	//	gb.Register.PC--
	//}
}

/*
 * GLOSSARY OF TERMS
 * cc		== condition
 * nn		== short immediate
 * n		== byte immediate
 * addr		== (HL)
 * byte 	== 8 bit register
 * short	== 16 bit register
 * union    == 2*8 bit registers joined
 */

func (gb *GameBoy) jp_nn(newAddress uint16) {
	gb.Register.PC = newAddress
}

func (gb *GameBoy) jr_n() {
	offset := int8(gb.nextByte()) 
	gb.Register.PC += uint16(offset)
}

func (gb *GameBoy) jr_z_n() {
	addr := gb.Register.PC
	val := int8(gb.nextByte())
	if gb.Register.IsSet(Z) {
		gb.Register.PC = addr + uint16(val)
	}
}

func (gb *GameBoy) jr_nz_n() {
	addr := gb.Register.PC
	val := int8(gb.nextByte())
	if !gb.Register.IsSet(Z) {
		gb.Register.PC = addr + uint16(val)
	}
}

func (gb *GameBoy) jr_c_n() {
	addr := gb.Register.PC
	val := int8(gb.nextByte())
	if gb.Register.IsSet(C) {
		gb.Register.PC = addr + uint16(val)
	}
}

func (gb *GameBoy) jr_nc_n() {
	addr := gb.Register.PC
	val := int8(gb.nextByte())
	if !gb.Register.IsSet(C) {
		gb.Register.PC = addr + uint16(val)
	}
}

func (gb *GameBoy) cp(reg *uint8) {
	val := gb.nextByte()
	output := *reg - val
	if output == 0 {
		gb.Register.F |= Z
	} else if output < 0 {
		gb.Register.F |= C
	}
	if output&0x10 == 0x10 {
		gb.Register.F |= H
	}
	gb.Register.F |= N
}

func (gb *GameBoy) cp_addr(lreg *uint8, rreg *uint8) {
	val := gb.nextByte()
	output := composeShort(*lreg, *rreg) - uint16(val)
	if output == 0 {
		gb.Register.F |= Z
	} else if output < 0 {
		gb.Register.F |= C
	}
	if output&0x10 == 0x10 {
		gb.Register.F |= H
	}
	gb.Register.F |= N
}

func (gb *GameBoy) scf() {
	gb.Register.Set(C)
}

func (gb *GameBoy) rrc(reg *uint8) {
	cVal := gb.Register.F&C == C
	rVal := *reg&0x01 == 0x01

	*reg = bits.RotateLeft8(*reg, -1)
	if cVal {
		*reg |= 0x80
	} else {
		*reg &^= 0x7F
	}

	if rVal {
		gb.Register.F |= 0x10
	} else {
		gb.Register.F &^= 0xEF
	}

	if *reg == 0 {
		gb.Register.Set(Z)
	}
	gb.Register.Unset(N)
	gb.Register.Unset(H)
}

func (gb *GameBoy) rlc(reg *uint8) {
	cVal := gb.Register.F&C == C
	rVal := *reg&0x80 == 0x80

	*reg = bits.RotateLeft8(*reg, -1)
	if cVal {
		*reg |= 0x01
	} else {
		*reg &^= 0xFE
	}

	if rVal {
		gb.Register.F |= C
	} else {
		gb.Register.F &^= 0xEF
	}

	if *reg == 0 {
		gb.Register.Set(Z)
	}
	gb.Register.Unset(N)
	gb.Register.Unset(H)
}

func (gb *GameBoy) ld_sp_addr() {
	address := gb.nextShort()
	gb.Memory.WriteShort(address, gb.Register.SP)
}

func (gb *GameBoy) rra() {
	carry := gb.Register.A & 0x01
	gb.Register.A = uint8(bits.RotateLeft(uint(gb.Register.A), -1))
	if gb.Register.A == 0 {
		gb.Register.F |= Z
	}
	gb.Register.F &^= N
	gb.Register.F &^= H
	if carry == 0x01 {
		gb.Register.F |= C
	} else {
		gb.Register.F &^= C
	}
}

func (gb *GameBoy) ei() {
	gb.Execute()
	gb.Interrupt.Master = 0
}

func (gb *GameBoy) di() {
	gb.Execute()
	gb.Interrupt.Master = 0
}

func (gb *GameBoy) ld_byte_n(register *uint8) {
	val := gb.nextByte()
	*register = val
}

func (gb *GameBoy) ld_union_n(lreg *uint8, rreg *uint8) {
	value := gb.nextShort()
	gb.Register.WriteShort(lreg, rreg, value)
}

func (gb *GameBoy) ld_byte_addr(reg *uint8) {
	addr := composeShort(gb.Register.H, gb.Register.L)
	*reg = gb.Memory.ReadByte(addr)
}

func (gb *GameBoy) ld_short_nn(reg *uint16) {
	value := gb.nextShort()
	*reg = value
}

func (gb *GameBoy) sub_addr(reg *uint8) {
	gb.Register.A -= *reg
	if gb.Register.A == 0 {
		gb.Register.Set(Z)
	}
	gb.Register.Set(N)
	if gb.Register.A&0x04 != 0x04 {
		gb.Register.Set(C)
		gb.Register.Set(H)
	}
}

func (gb *GameBoy) add_byte(lreg *uint8, rreg *uint8) {
	*lreg += *rreg
}

func (gb *GameBoy) adc(reg *uint8) {
	gb.Register.A = *reg & 0x04
}

func (gb *GameBoy) adc_hl() {
	addr := composeShort(gb.Register.H, gb.Register.L)
	gb.Register.A = gb.Memory.ReadByte(addr) + 0x40
	if gb.Register.A == 0 {
		gb.Register.Set(Z)
	}
	gb.Register.Unset(N)
	if gb.Register.A&0x40 == 0x40 {
		gb.Register.Set(H)
	}
	if gb.Register.A&0x01 == 0x01 {
		gb.Register.Set(C)
	}
}

func (gb *GameBoy) cpl() {
	gb.Register.A = ^gb.Register.A
	gb.Register.Set(N)
	gb.Register.Set(H)
}

func (gb *GameBoy) add_short_addr(reg *uint16) {
	left := *reg
	right := composeShort(gb.Register.H, gb.Register.L)
	H, L := splitShort(left + right)
	gb.Register.H = H
	gb.Register.L = L

	gb.Register.Unset(N)
	HL := composeShort(gb.Register.H, gb.Register.L)
	if HL&0x8000 == 0x8000 {
		gb.Register.Set(C)
	}
	if HL&0x800 == 0x800 {
		gb.Register.Set(H)
	}
}

func (gb *GameBoy) add_short(lrega *uint8, rrega *uint8) {
	left := composeShort(*lrega, *rrega)
	right := composeShort(gb.Register.H, gb.Register.L)
	H, L := splitShort(left + right)
	gb.Register.H = H
	gb.Register.L = L

	gb.Register.Unset(N)
	HL := composeShort(gb.Register.H, gb.Register.L)
	if HL&0x8000 == 0x8000 {
		gb.Register.Set(C)
	}
	if HL&0x800 == 0x800 {
		gb.Register.Set(H)
	}
}

func (gb *GameBoy) ld_addr_nn_byte(addl *uint8, addr *uint8, reg *uint8) {
	address := composeShort(*addl, *addr)
	*reg = gb.Memory.ReadByte(address)
}

func (gb *GameBoy) ld_byte_byte(dest *uint8, source *uint8) {
	*dest = *source
}

func (gb *GameBoy) ld_addr() {
	addr := gb.nextShort()
	gb.Register.A = gb.Memory.ReadByte(addr)
}

func (gb *GameBoy) ld_addr_ram(lreg *uint8, rreg *uint8) {
	addr := composeShort(*lreg, *rreg)
	gb.Register.A = gb.Memory.ReadByte(addr)
}

func (gb *GameBoy) ld_comp_addr_A(lreg *uint8, rreg *uint8) {
	address := composeShort(*lreg, *rreg)
	fmt.Printf("Loading 0x%X into address 0x%X\n", gb.Register.A, address)
	fmt.Printf("Loading 0x%X into address 0x%X\n", gb.Register.A, address+1)
	gb.Memory.WriteByte(address, gb.Register.A)
}

func (gb *GameBoy) ld_nn_addr_a() {
	addr := gb.nextShort()
	gb.Memory.WriteByte(addr, gb.Register.A)
}

func (gb *GameBoy) inc(reg *uint8) {
	*reg++
	if *reg == 0 {
		gb.Register.F |= Z
	}
	gb.Register.F &^= N
	if *reg&0x04 == 0x04 {
		gb.Register.F |= H
	}
}

func (gb *GameBoy) ccf() {
	gb.Register.Unset(C)
}

func (gb *GameBoy) inc_short_addr(reg *uint16) {
	*reg++
	if *reg == 0 {
		gb.Register.F |= Z
	}
	gb.Register.F &^= N
	if *reg&0x04 == 0x04 {
		gb.Register.F |= H
	}
}

func (gb *GameBoy) dec_hl() {
	address := composeShort(gb.Register.H, gb.Register.L)
	sub := gb.Memory.ReadByte(address)
	sub--
	gb.Memory.WriteByte(address, sub)
	if sub == 0 {
		gb.Register.F |= Z
	}
	gb.Register.Set(N)
	if sub&0x04 == 0x04 {
		gb.Register.F |= H
	}
}

func (gb *GameBoy) dec_short_addr(reg *uint16) {
	*reg--
	if *reg == 0 {
		gb.Register.F |= Z
	}
	gb.Register.Set(N)
	if *reg&0x04 == 0x04 {
		gb.Register.F |= H
	}
}

func (gb *GameBoy) inc_short(lreg *uint8, rreg *uint8) {
	shVal := composeShort(*lreg, *rreg)
	shVal++
	*lreg, *rreg = splitShort(shVal)
	if shVal == 0 {
		gb.Register.Set(Z)
	}
	gb.Register.Unset(N)
	if shVal&0x04 == 0x04 {
		gb.Register.Set(H)
	}
}

func (gb *GameBoy) dec_short(lreg *uint8, rreg *uint8) {
	val := composeShort(*lreg, *rreg)
	val--
	L, R := splitShort(val)
	*lreg = L
	*rreg = R
}

func (gb *GameBoy) dec(reg *uint8) {
	if (*reg & 0x0F) != 0 {
		gb.Register.Unset(H)
	} else {
		gb.Register.Set(H)
	}

	*reg--

	if *reg > 0 {
		gb.Register.Unset(Z)
	} else {
		gb.Register.Set(Z)
	}
	gb.Register.Set(N)
}

func (gb *GameBoy) ret() {
	stackVal := gb.Memory.ReadShort(gb.Register.SP)
	gb.Register.SP += 2
	gb.Register.PC = stackVal
}

func (gb *GameBoy) jp_cc_nn(flag uint8, set bool) {
	val := bits.Reverse16(gb.nextShort())
	if gb.Register.IsSet(flag) {
		if set {
			gb.Register.PC = uint16(val)
			//fmt.Printf("CONDITION: %08b IS %v\nF VALUE: %08b\nMATCH: %v\n", flag, set, gb.Register.F, true)
			//fmt.Printf("Jumping to %#x, PC + %#x\n", gb.Register.PC, val)
		}
	} else {
		if !set {
			//fmt.Printf("CONDITION: %08b IS %v\nF VALUE: %08b\nMATCH: %v\n", flag, set, gb.Register.F, false)
			gb.Register.PC = uint16(val)
			//fmt.Printf("Jumping to %#x, PC + %#x\n", gb.Register.PC, val)
		}
	}
}

func (gb *GameBoy) ldh_byteaddr_a(reg *uint8) {
	addr := uint16(*reg) + 0xFF00
	gb.Memory.WriteByte(addr, gb.Register.A)
}

func (gb *GameBoy) ldh_n_a() {
	val := gb.nextByte()
	gb.Memory.WriteByte(0xFF00+uint16(val), gb.Register.A)
}

func (gb *GameBoy) ldh_a_n() {
	val := gb.nextByte()
	gb.Register.A = gb.Memory.ReadByte(0xFF00 + uint16(val))
}

func (gb *GameBoy) or(reg *uint8) {
	val := gb.nextByte()
	res := val | *reg
	*reg = res
	if *reg == 0 {
		gb.Register.Set(Z)
	} else {
		gb.Register.Unset(Z)
	}
	gb.Register.Unset(N | H | C)
}

func (gb *GameBoy) xor(reg *uint8, value uint8) {
	*reg ^= value
	if *reg == 0 {
		gb.Register.Set(Z)
	} else {
		gb.Register.Unset(Z)
	}
	gb.Register.Unset(N | H | C)
}

func (gb *GameBoy) call_nn(address uint16) {
	gb.Register.SP = gb.Register.PC + 2
	gb.jp_nn(gb.Memory.ReadShort(gb.Register.PC + 1))
}
