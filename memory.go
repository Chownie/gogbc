package main

import (
	"encoding/binary"
	"math/rand"
	"time"
	"fmt"
)

// Memory contains GB system memory
type Memory struct {
	cart [0x8000]byte
	sram [0x2000]byte
	io   [0x100]byte
	vram [0x2000]byte
	oam  [0x100]byte
	wram [0x2000]byte
	hram [0x80]byte
}

func (mem *Memory) ReadShort(address uint16) uint16 {
	return binary.LittleEndian.Uint16([]uint8{mem.ReadByte(address), mem.ReadByte(address + 1)})
}

func (mem *Memory) WriteShort(address uint16, value uint16) {
	L, R := splitShort(value)
	mem.WriteByte(address, L)
	mem.WriteByte(address+1, R)
}

func (mem *Memory) GetVRAM() [0x2000]byte {
	return mem.vram
}

func (mem *Memory) ReadByte(address uint16) byte {
	if address <= 0x7fff {
		return mem.cart[address]
	} else if address >= 0xa000 && address <= 0xbfff {
		return mem.sram[address-0xa000]
	} else if address >= 0x8000 && address <= 0x9fff {
		return mem.vram[address-0x8000]
	} else if address >= 0xc000 && address <= 0xdfff {
		return mem.wram[address-0xc000]
	} else if address >= 0xe000 && address <= 0xfdff {
		return mem.wram[address-0xe000]
	} else if address >= 0xfe00 && address <= 0xfeff {
		return mem.oam[address-0xfe00]

		// Should return a div timer, but a random number works just as well for Tetris
	} else if address == 0xff04 {
		rand.Seed(time.Now().UnixNano())
		return byte(rand.Intn(255))
	} else if address >= 0xff80 && address <= 0xfffe {
		return mem.hram[address-0xff80]
	}

	return 0
}

func (mem *Memory) Write(address uint16, value []byte) {
	for i, b := range value {
		mem.WriteByte(uint16(i)+address, b)	
	}
}

// WriteByte writes a single byte to a selected address
// breaks addresses into specific chunks of GB memory
func (mem *Memory) WriteByte(address uint16, value uint8) {
	if address <= 0x7fff {
		mem.cart[address] = value
	} else if address >= 0xa000 && address <= 0xbfff {
		mem.sram[address-0xa000] = value
	} else if address >= 0x8000 && address <= 0x9fff {
		fmt.Println("WRITE:\tWriting to VRAM")
		mem.vram[address-0x8000] = value
	} else if address >= 0xc000 && address <= 0xdfff {
		mem.wram[address-0xc000] = value
	} else if address >= 0xe000 && address <= 0xfdff {
		mem.wram[address-0xe000] = value
	} else if address >= 0xfe00 && address <= 0xfeff {
		//fmt.Println("WRITE:\tWriting to OAM")
		mem.oam[address-0xfe00] = value
	}
}
