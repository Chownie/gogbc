package main

// Carry Flags
const (
	Z = 128
	N = 64
	H = 32
	C = 16
)

// Registers for gameboy CPU
type Registers struct {
	A  uint8
	B  uint8
	C  uint8
	D  uint8
	E  uint8
	F  uint8
	H  uint8
	L  uint8
	SP uint16
	PC uint16
	M  uint8
	T  uint8
}

func (reg *Registers) IsSet(flag uint8) bool {
	return reg.F&flag == flag
}

func (reg *Registers) Set(flag uint8) {
	reg.F |= flag
}

func (reg *Registers) Unset(flag uint8) {
	reg.F &^= flag
}

func (reg *Registers) WriteShort(lreg *uint8, rreg *uint8, value uint16) {
	left, right := splitShort(value)
	*lreg = left
	*rreg = right
}
