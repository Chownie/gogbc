package main

// TODO: READ THESE

// http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-Graphics
// http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-GPU-Timings

// GPU emulation struct
type GPU struct {
	Mode		int
	Modeclock 	int
	Line 		int
}

func (gpu *GPU) Step(ticks int) {
	gpu.Modeclock += ticks

	switch(gpu.Mode) {
		// Accessing OAM
		case 2:
			if gpu.Modeclock >= 80 {
				gpu.Modeclock = 0
				gpu.Mode = 3
			}
	// Scanline, accessing VRAM
		case 3:
			if gpu.Modeclock >= 172 {
				gpu.Modeclock = 0
				gpu.Mode = 0

				gpu.Renderscan()
			}
	// Hblank
		case 0:
			if gpu.Modeclock >= 204 {
				gpu.Modeclock = 0
				gpu.Line += 1

				if gpu.Line == 143 { // VBlank
					gpu.Mode = 1
					// gpu draw image data to the screen psuedo gpu.Draw(0, 0)
				} else {
					gpu.Mode = 2
				}
			}
	// Vblank (10 lines)
		case 1:
			if gpu.Modeclock >= 456 {
				gpu.Modeclock = 0
				gpu.Line += 1

				if gpu.Line > 153 {
					// Restart
					gpu.Mode = 2
					gpu.Line = 0
				}
			}
	}
}

func (gpu *GPU) Renderscan() {
	// TODO
}