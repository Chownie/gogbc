package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"encoding/hex"
	"encoding/binary"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

var font *ttf.Font

func RenderMultiline(target *sdl.Surface, text []string, x int, y int) {
	for number, line := range text {
		solid, err := font.RenderUTF8Solid(line, sdl.Color{255, 255, 255, 255})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to render text: %s\n", err)
			return
		}
		rect := &sdl.Rect{0, 0, solid.W, solid.H}
		solid.Blit(rect, target, &sdl.Rect{int32(x), int32(y + (font.Height() * number)), 0, 0})
		solid.Free()
	}
}

func renderdebug(target *sdl.Surface, gb *GameBoy) {
	debuginfo := fmt.Sprintf(
		"AF: %02X%02X\nBC: %02X%02X\nDE: %02X%02X | M: %02X\nHL: %02X%02X | T: %02X\nSP: %04X | PC: %04X",
		gb.Register.A,
		gb.Register.F,
		gb.Register.B,
		gb.Register.C,
		gb.Register.D,
		gb.Register.E,
		gb.Register.M,
		gb.Register.L,
		gb.Register.H,
		gb.Register.T,
		gb.Register.SP,
		gb.Register.PC)
	RenderMultiline(target, strings.Split(debuginfo, "\n"), 0, 220)
}

func main() {
	var event sdl.Event
	var err error

	if err := ttf.Init(); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to initialize TTF: %s\n", err)
	}

	if font, err = ttf.OpenFont("input.ttf", 12); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open font: %s\n", err)
	}
	defer font.Close()

	ROM, err := ioutil.ReadFile("tetris.gb")

	if err != nil {
		panic(err)
	}

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		160, 300, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	surface, err := window.GetSurface()
	if err != nil {
		panic(err)
	}

	GB := new(GameBoy)
	GB.LoadROM(ROM)
	GB.Register.PC = 0x0100

	running := true
	cont := false
	var target uint16
	target = 0

	if len(os.Args) > 1 {
		targetBytes, err := hex.DecodeString(os.Args[1])
		if err != nil {
			fmt.Println(err)
		}
		target = binary.BigEndian.Uint16(targetBytes)
		fmt.Println("BREAKPOINT: ", target)
	}


	for running {
		for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.KeyboardEvent:
				if t.Type == sdl.KEYUP {
					if t.Keysym.Sym == sdl.K_RETURN {
						go GB.Execute()
					}
					if t.Keysym.Sym == sdl.K_F9 {
						cont = true
					}
				}
			}
		}

		if cont {
			GB.Execute()
			if GB.Register.PC == target {
				cont = false
			}
		}

		rect := sdl.Rect{0, 0, 200, 300}
		surface.FillRect(&rect, 0xffff0000)

		renderdebug(surface, GB)

		window.UpdateSurface()
		if sdl.GetTicks() < (1000 / 60) {
			sdl.Delay((1000 / 60) - sdl.GetTicks())
		}
	}
}
