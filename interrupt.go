package main

type Interrupt struct {
	Master uint8
	Enable uint8
	Flags  uint8
}
